#!/bin/bash

### Set initial time of file
LTIME='stat -c %Z /localhost/aetvdigital/wp-content/themes/aetvdigital/sass/aetvdigital.scss'

while true    
do
   ATIME='stat -c %Z /localhost/aetvdigital/wp-content/themes/aetvdigital/sass/aetvdigital.scss'

   if [[ "$ATIME" != "$LTIME" ]]
   then    
       echo "RUN COMMNAD"
       compass clean && compass compile

       LTIME=$ATIME
   fi
   sleep 5
done

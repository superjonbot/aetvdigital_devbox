module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
//        sass: {
//            dist: {
//                files: {
//                    'stylesheets/style.css' : 'sass/style.scss'
//                }
//            }
//        },
//        compass: {
//            dist: {
//                options: {
//                    sassDir: 'sass',
//                    cssDir: 'stylesheets'
//                }
//            }
//        },

        shell: {
            options: {
                stderr: false
            },
            target: {
                command: 'compass clean && compass compile'
            }
        },


        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['shell'],
                options: {
                    livereload: {
                        port: 9000
                    }
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');

    grunt.registerTask('default',['watch']);
}
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package aetvdigital
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer grid_10 push_1" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'aetvdigital' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'aetvdigital' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'aetvdigital' ), 'aetvdigital', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/global_foot.js"></script>
</body>
</html>

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aetvdigital');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admLn**');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fE.|<sQY^:ZT>@E=bi@M4X i-e%fMr{DS$I8o 4y2J~P.nO@J&PpZ+i|o636O]5(');
define('SECURE_AUTH_KEY',  'nvTS9^m;6)xi>@>&1,-moiilQej;#m=+&0?WXT91pTG(?Xo(:4o51#~$O@pL*@jH');
define('LOGGED_IN_KEY',    ':LA VxrJ9q*p4e8,^)1`Ik=L56m_,==!N7b=R1FXO&5p6kQ&gsB*zE3?wYm|B+6Q');
define('NONCE_KEY',        '#;}l0ZD=j6N[}KS1TTQOKaz/#tc:@,$7D81M>KT^M}mp!#8$kLQ:+Ot1Ip{cSbCo');
define('AUTH_SALT',        'Nav7Li5=*!LV!uVG%Z8yzs$4j8RA1TCWiy>x,1G> `<4+)mE330oI6G1=%]&+n8b');
define('SECURE_AUTH_SALT', 'iu<w~?`?m8c>eN%l>x }>?RI`S(xbyWvXuP}W2J@e,hjHY(X2d6842Zo~N+76ye7');
define('LOGGED_IN_SALT',   'A0oIzjf4ClO?a:(HJ4Rmea>c;I(/d]}c;C0;`Fei4#d5(_!,mcOUT4v}1=-~9@-)');
define('NONCE_SALT',       'P0Wfc<]Vw}V&qh *Q*8+*&Ie5h=L9#}M|lqyb+b@(h>AG)#U<: ule8ha%m8DFyB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

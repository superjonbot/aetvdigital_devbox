SETUP
-----

prerequisites:
-vagrant
-virtual box
-virtual box guest additions

in your machine's hostfile add:
192.168.0.169   help
192.168.0.169   local.aetvdigital.com

Clone vagrant box repo to your <devbox directory>:
	https://github.com/superjonbot/vagrant_workdevbox2

in the <devbox directory>/localhost directory, clone this repo to "aetvdigital"	

START VAGRANT BOX
> cd <devbox directory>/_OperaBox
> vagrant up --provider=virtualbox

RESTORE DATABASE
> vagrant ssh
> cd /localhost/aetvdigital/SQL
> gpg --decrypt-files *.gpg (password is the same as stagingae password)
> bash restoredatabase.sh

The site should now be available at:
http://local.aetvdigital.com/
The box setup can be seen at:
http://help/
phpMyadmin is accessible at:
http://help/sql
